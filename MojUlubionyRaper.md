---
marp: true
author: Antoni Jarczewski
size: 4:3
theme: gaia
---
<style>
    :root{
        --cloro-background: #101010;
        --color-background: #FFFFFF; 
    }
    h1 {
    font-family: Courier New;
    
}
</style>

# **Eminem**
```
Krótka prezentacja mojego ulubionego rapera
````
![Picture](eminem.jpg)

---
### Ciekawostki o raperze
- prawdziwe nazwisko Marshall Bruce Mathers
- Eminem sprzedał ponad 225 milionów płyt na całym świecie, co czyni go jednym z artystów, którzy sprzedali najwięcej płyt
- Wciąż nieznany szerokiej publiczności, Eminem wydał swój pierwszy album, Infinite , w 1996 roku .
- Ma ponad 600 nagród, w tym 15 nagród Grammy i Oscara .
- Jest ojcem Hallie Jade Scott Mathers oraz przybranym ojcem dwóch innych córek

---
 Najpopularniejsze albumy *Eminema*
##### **1.The Marshal Mathers LP (2001)**
![drawing](TheMarshallMathersLP.png)

---
 Najpopularniejsze albumy *Eminema*
#### **2. The Eminem Show (2002)**
![drawing](The_Eminem_Show.jpg)

---
 Najpopularniejsze albumy *Eminema*
 ### **3.Recovery (2010)**
 ![drawing](Recovery_Album_Cover.jpg)

---

---

# Dziękuje za uwagę :)